export default {
  primary: '#1976D2',
  secondary: '#4caf50', // verde
  tertiary: '#495057', // cinza
  accent: '#82B1FF', // azul claro
  error: '#f55a4e', // vermelho
  info: '#00d3ee', // azul cyan
  success: '#5cb860', // verde
  warning: '#ffa21a', // laranja
  testes: '#a6c4f4',
  danger: '#f55a4e'
}
