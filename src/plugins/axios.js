/* eslint-disable no-unused-expressions */
import axios from 'axios'
import store from '@/store/index'
import router from '@/router/index'
import { api } from '@/config'
import { app } from '@/main'

axios.interceptors.request.use(
  (config) => {
    // config.headers['X-Requested-With'] = 'XMLHttpRequest'
    config.headers['Content-Type'] = 'application/json'
    // config.headers["Content-Type"] = "application/x-www-form-urlencoded";

    const token = store.getters['auth/token']
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }

    return config
  },
  error => Promise.reject(error)
)

axios.interceptors.response.use(
  response => response,
  async (error) => {
    if (store.getters['auth/token']) {
      // TODO: Melhorar validação de token
      if (error.response.status === 401 && error.response.data.message === 'Token expirado!') {
        const { data } = await axios.post(api.path('login.refresh'))
        store.dispatch('auth/saveToken', data)
        return axios.request(error.config)
      }

      if (
        error.response.status === 401 ||
        (error.response.status === 500 &&
          (error.resopnse.data.message === 'Token expirado e não pode mais ser atualizado' ||
            error.resopnse.data.message === 'Este token não é mais válido.'))
      ) {
        store.dispatch('auth/destroy')
        router.push({ name: 'acesso' })
      }
    }

    error.response.data.message !== undefined &&
      app.$toast.error(error.response.data.message || 'Alguma coisa deu errado :(')
    error.response.data.error !== undefined &&
      app.$toast.error(error.response.data.error || 'Um erro ocorreu nessa requisição :(')
    return Promise.reject(error)
  }
)
