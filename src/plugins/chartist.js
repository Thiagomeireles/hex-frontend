import Vue from 'vue'
import 'chartist/dist/chartist.min.css'
import 'chartist-plugin-tooltips-updated'

Vue.use(require('vue-chartist'))
