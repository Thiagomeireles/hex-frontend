import { set, toggle } from '@/utils/vuex'

export default {
  setDrawer: set('drawer'),
  setImage: set('image'),
  setDark: set('dark'),
  setColor: set('color'),
  toggleDrawer: toggle('drawer')
}
