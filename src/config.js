// const BASE_URL = `${window.origin}/api`; // siteUrl = Laravel.siteUrl,
const BASE_URL = 'http://hex-api.test/api' // siteUrl = Laravel.siteUrl,

export const settings = {
  siteName: 'Hex'
}

class URL {
  constructor (base) {
    this.base = base
  }

  path (path, args) {
    // eslint-disable-next-line no-param-reassign
    path = path.split('.')
    let obj = this
    let url = this.base

    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < path.length && obj; i++) {
      if (obj.url) {
        url += `/${obj.url}`
      }

      obj = obj[path[i]]
    }

    if (obj) {
      url = `${url}/${typeof obj === 'string' ? obj : obj.url}`
    }

    if (args) {
      Object.keys(args).forEach((key) => {
        url = url.replace(`:${key}`, args[key])
      })

      // for (const key in args) {
      //   url = url.replace(`:${key}`, args[key]);
      // }
    }

    // console.log(url)

    return url
  }
}

export const api = Object.assign(new URL(BASE_URL), {
  testes: 'testes',
  url: '',

  dashboard: { url: 'vulnerability/dashboard' },

  // AUTH
  login: { url: 'auth/login', refresh: 'refresh' },
  acesso: { url: 'acesso' },
  acessoLogin: { url: 'acessoLogin' },
  identificacaoPositiva: { url: 'identificacaoPositiva' },
  definicoesSeguranca: { url: 'definicoesSeguranca' },
  redefinicaoSenha: { url: 'redefinicaoSenha' },

  logout: 'logout',
  register: 'register',
  password: { url: 'password', forgot: 'email', reset: 'reset' },
  me: 'me',
  // users: {
  //   url: "users",
  //   activate: ":id/activate",
  //   single: ":id",
  //   restore: ":id/restore"
  // },
  profile: { url: 'profile' },
  updateProfile: { url: 'updateProfile' },
  updatePassword: { url: 'updatePassword' },
  home: { url: 'home' },
  gerarCodigo: { url: 'gerarcodigo' },
  confirmarCodigo: { url: 'confirmarcodigo' },

  usuario: {
    url: 'usuario',
    cadastrar: 'cadastrar',
    editar: 'editar/:id',
    ativar: 'ativar/:id',
    inativar: 'inativar/:id',
    excluir: 'excluir/:id'
  },

  carga: {
    url: 'carga',
    usuario: {
      url: 'usuario',
      file: 'file',
      filepath: 'filepath'
    },
    holerite: {
      url: 'holerite',
      file: 'file',
      filepath: 'filepath'
    }
  },

  empresa: {
    url: 'empresa'
  },

  arquivo: {
    url: 'arquivo',
    detalhes: 'detalhes/:arquivo_id'
  },

  grupo: {
    url: 'grupo'
  },

  holerite: {
    url: 'holerite',
    imprimir: 'imprimir',
    // download: "download/:year/:month",
    download: 'download',
    visualizar: 'visualizar/:id'
  },

  imprimir: {
    url: 'imprimir'
  },

  funcionario: {
    url: 'funcionario',
    holeritesByYear: 'holeritesByYear/:year',
    getMonthYears: 'getMonthYears'
  },

  layout: 'layout/:tipo'
})
