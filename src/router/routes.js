function applyRules (rules, routes) {
  Object.keys(routes).forEach((key) => {
    routes[key].meta = routes[key].meta || {}

    if (!routes[key].meta.rules) {
      routes[key].meta.rules = []
    }
    routes[key].meta.rules.unshift(...rules)

    if (routes[key].children) {
      routes[key].children = applyRules(rules, routes[key].children)
    }
  })

  // for (const i in routes) {
  //   console.log('i => ', i);
  //   // console.log('TCL: applyRules -> routes', routes)
  //   routes[i].meta = routes[i].meta || {};
  //   // console.log('TCL: applyRules -> routes[i].meta', routes[i].meta)

  //   if (!routes[i].meta.rules) {
  //     routes[i].meta.rules = [];
  //   }
  //   routes[i].meta.rules.unshift(...rules);

  //   if (routes[i].children) {
  //     routes[i].children = applyRules(rules, routes[i].children);
  //   }
  // }

  return routes
}

export default [
  ...applyRules(
    ['guest'],
    [
      {
        path: '',
        component: () => import(/* webpackChunkName: "routes" */ '@/components/auth/AuthWrapper.vue'),
        redirect: { name: 'Login' },
        children: [
          // Novas rotas`
          {
            path: '/login',
            name: 'Login',
            component: () => import(/* webpackChunkName: "routes" */ '@/components/auth/login/Login.vue')
          }
          // {
          //   path: '/password',
          //   component: () => import(/* webpackChunkName: "password" */ '@/components/auth/password/PasswordWrapper.vue'),
          //   children: [
          //     {
          //       path: '',
          //       name: 'forgot',
          //       component: () => import(/* webpackChunkName: "passwordForgot" */ '@/components/auth/password/password-forgot/PasswordForgot.vue'),
          //     },
          //     {
          //       path: 'reset/:token',
          //       name: 'reset',
          //       component: () => import(/* webpackChunkName: "passwordReset" */ '@/components/auth/password/password-reset/PasswordReset.vue'),
          //     },
          //   ],
          // },
        ]
      }
    ]
  ),
  ...applyRules(
    // ["auth_admin"],
    // ['auth', 'gerencial'],
    ['guest'],
    [
      {
        path: '',
        component: () => import(/* webpackChunkName: "admin" */ '@/components/system/SystemWrapper.vue'),
        children: [
          { path: '', name: 'index', redirect: { name: 'Dashboard' } },
          // {
          //   path: "profile",
          //   component: () =>
          //     import("@/components/system/profile/ProfileWrapper.vue"),
          //   children: [
          {
            path: '/profile',
            name: 'Profile',
            component: () =>
              import('@/components/system/profile/UserProfile.vue')
          },
          //     {
          //       path: "edit",
          //       name: "profile-edit",
          //       component: () =>
          //         import("@/components/system/profile/edit/ProfileEdit.vue")
          //     }
          //   ]
          // },
          {
            path: '/dashboard',
            name: 'Dashboard',
            component: () => import(/* webpackChunkName: "admin" */ '@/components/system/views/Dashboard.vue')
          },
          {
            path: '/dashboard2',
            name: 'Dashboard2',
            component: () => import(/* webpackChunkName: "admin" */ '@/components/system/views/vulnerabilidades/Dashboard.vue')
          },
          {
            path: '/upload',
            name: 'Upload',
            component: () => import(/* webpackChunkName: "admin" */ '@/components/system/views/vulnerabilidades/Upload.vue')
          },
          {
            path: '/inclusao',
            name: 'Inclusao',
            component: () => import(/* webpackChunkName: "admin" */ '@/components/system/views/vulnerabilidades/Inclusao.vue')
          },
          // {
          //   path: '/icons',
          //   name: 'Icons',
          //   component: () => import(/* webpackChunkName: "admin" */ '@/components/system/views/Icons.vue')
          // },
          {
            path: '/notifications',
            name: 'Notifications',
            component: () => import(/* webpackChunkName: "admin" */ '@/components/system/views/Notifications.vue')
          },
          {
            path: '/lista',
            name: 'Lista',
            component: () => import(/* webpackChunkName: "admin" */ '@/components/system/views/vulnerabilidades/Lista.vue')
          }
          // {
          //   path: '/arquivos',
          //   name: 'arquivos',
          //   component: () => import(/* webpackChunkName: "admin" */ '@/components/system/views/Arquivos.vue'),
          // },
        ]
      }
    ]
  ),
  // ...applyRules(
  //   ['auth', 'funcionario'],
  //   [
  //     {
  //       path: '',
  //       // name: "meusholerites",
  //       component: () => import(/* webpackChunkName: "funcionario" */ '@/components/funcionario/Wrapper.vue'),
  //       children: [
  //         { path: '', name: 'indexFuncionario', redirect: { name: 'home' } },
  //         {
  //           path: 'meusholerites',
  //           name: 'home',
  //           component: () => import(/* webpackChunkName: "funcionario" */ '@/components/funcionario/Home.vue'),
  //         },
  //       ],
  //     },
  //   ],
  // ),
  { path: '*', redirect: { name: 'Dashboard' } }
]
