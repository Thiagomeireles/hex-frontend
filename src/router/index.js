import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/index'
import routes from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  // base: process.env.BASE_URL,
  base: '/',
  routes
})

const rules = {
  guest: { fail: 'index', check: () => !store.getters['auth/check'] },
  auth: { fail: 'acesso', check: () => store.getters['auth/check'] },
  gerencial: { fail: 'home', check: () => store.getters['auth/level'] > 10 },
  funcionario: { fail: 'index', check: () => store.getters['auth/check'] }
}

function reroute (to) {
  let failRoute = false
  let checkResult = false

  // eslint-disable-next-line no-unused-expressions
  to.meta.rules && to.meta.rules.forEach((rule) => {
    let check = false
    if (Array.isArray(rule)) {
      const checks = []

      Object.keys(rule).forEach((key) => {
        checks[key] = rules[rule[key]].check()
        check = check || checks[key]
      })

      // for (const i in rule) {
      //   checks[i] = rules[rule[i]].check();
      //   check = check || checks[i];
      // }
      if (!check && !failRoute) {
        failRoute = rules[rule[checks.indexOf(false)]].fail
        //
      }
    } else {
      check = rules[rule].check()
      if (!check && !failRoute) {
        failRoute = rules[rule].fail
      }
    }

    checkResult = checkResult && check
  })

  if (!checkResult && failRoute) {
    //
    return { name: failRoute }
  }

  return false
}

router.beforeEach(async (to, from, next) => {
  if (store.getters['auth/token'] && !store.getters['auth/check']) {
    try {
      await store.dispatch('auth/fetchUser')
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
  }

  const route = reroute(to)

  if (route) {
    next(route)
  } else {
    next()
  }
})

export default router
