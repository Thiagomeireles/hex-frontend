module.exports = {
  devServer: {
    historyApiFallback: true,
    hot: true,
    host: '0.0.0.0',
    port: 9521,
    disableHostCheck: true,
    // https: true,
    // public: "https://holeriteteste.local",
    proxy: {
      '/api': {
        target: 'http://hex-api.test',
        changeOrigin: true
      }
    }
  },

  productionSourceMap: false,

  pwa: {
    name: 'HEX'
  }
}
